from django.shortcuts import render
from django.views.generic import ListView, TemplateView
from django.views.decorators.http import require_POST
from django.http import HttpResponse

from videos.models import Video, VideoStat


class ListVideos(ListView):

    model = Video
    template_name = 'videos/player.html'


@require_POST
def push_stat(request):
    post = request.POST
    video_id = post.get('video_id')
    stat_type = post.get('stat_type')

    video = Video.objects.get(video_id=video_id)
    vid_stat, created = VideoStat.objects.get_or_create(video=video)
    if stat_type == "loads":
        vid_stat.loads += 1
    elif stat_type == "plays":
        vid_stat.plays += 1

    vid_stat.save()

    return HttpResponse()
