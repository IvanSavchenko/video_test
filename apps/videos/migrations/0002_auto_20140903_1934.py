# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='videostat',
            name='loads',
            field=models.IntegerField(default=0, max_length=200),
        ),
        migrations.AlterField(
            model_name='videostat',
            name='plays',
            field=models.IntegerField(default=0, max_length=200),
        ),
    ]
